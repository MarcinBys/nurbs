﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Interfejs : MonoBehaviour {
    
    public InputField punktKontrolnyX;
    public InputField punktKontrolnyY;
    public InputField wagi;
    public InputField wezly;

    public Slider stopienKrzywej;
    public Text pokazStopien;

    public Button DodajPunktKontrolny_Button;
    public Button ZatwierdzPunkty_Button;
    public Button ZapiszWW_Button;
    public Button Rysuj_Button;

    private Krzywa krzywa;

    void Start () {
        krzywa = GetComponent<Krzywa>();

        // wylacz interaktywnosc niepotrzebneych przyciskow i pol tekstowych:
        ZapiszWW_Button.interactable = false;
        Rysuj_Button.interactable = false;
        stopienKrzywej.interactable = false;
        wagi.interactable = false;
        wezly.interactable = false;
    }

    /// <summary>
    /// Obsluga przycisku dodajacego punkt kontrolny do sceny
    /// </summary>
    public void Przycisk_StworzPunktKontrolny()
    {
        try
        {
            // Odczytaj wartosci punktow kontrolnych z pol i stworz punkt na scenie
            krzywa.StworzPunktKontrolny(new Vector2(float.Parse(punktKontrolnyX.text, CultureInfo.InvariantCulture), float.Parse(punktKontrolnyY.text, CultureInfo.InvariantCulture)));

            // Wyczysc pola tekstowe
            punktKontrolnyX.text = "";
            punktKontrolnyY.text = "";
        }
        catch
        {
            Debug.Log("Bledne dane punktu kontrolnego.");
        }
    }
    
    /// <summary>
    /// Funkcja wywolujaca inne funkcje po nacisnieciu wybranego przycisku
    /// </summary>
    public void Przycisk_ZatwierdzPunktyKontrolne()
    {
        krzywa.ZatwierdzPunktyKontrolne();      // wywolaj funkcje obliczajaca i zapisujaca podstawowe parametry krzywej NURBS 

        UstawMaxStopien();                      // ustaw wartosc maksymalna przedzialu na suwaku

        // ustaw tekst w polach tekstowych obiektow InputField
        WyswietlWagi();
        WyswietlWezly();

        // ustaw interaktywnosc przyciskow
        ZapiszWW_Button.interactable = true;
        Rysuj_Button.interactable = true;
        DodajPunktKontrolny_Button.interactable = false;
        ZatwierdzPunkty_Button.interactable = false;
        stopienKrzywej.interactable = true;

        // ustaw interaktywnosc pol tekstowych
        punktKontrolnyX.interactable = false;
        punktKontrolnyY.interactable = false;
        wagi.interactable = true;
        wezly.interactable = true;
    }

    /// <summary>
    /// Ustawia maksymalna wartosc przedzialu na suwaku
    /// </summary>
    public void UstawMaxStopien()
    {
        stopienKrzywej.maxValue = krzywa.max_n;
    }

    /// <summary>
    /// Funkcja wywolywana przez przesuniecie suwaku okreslajacego aktualnie wybrany stopien krzywej
    /// </summary>
    public void UstawAktuanyStopien()
    {
        krzywa.n = int.Parse(stopienKrzywej.value.ToString());      // zapisz wybrany stopien do zmiennej przechowujacej aktualny stopien krzywej NURBS

        pokazStopien.text = "Stopien: " + krzywa.n.ToString();      // wyswietl wybrany stopien obok suwaka

        Przycisk_ZatwierdzPunktyKontrolne();                        // zaktualizuj parametry krzywej
    }
    
    /// <summary>
    /// Wyswietl tablice wag w polu tekstowym obiektu InputField
    /// </summary>
    private void WyswietlWagi()
    {
        wagi.text = "";         // wyczysc pole tekstowe

        foreach(float waga in krzywa.w)
        {
            wagi.text += waga.ToString() + " ";
        }
    }

    /// <summary>
    /// Zapisz wagi z pola tekstowego obiektu InputField do tablicy przechowujacej wagi punktow kontrolnych
    /// </summary>
    public void ZapiszWagi()
    {
        string[] liczby = wagi.text.Split(' ');     // podziel stringa na tablice stringow

        for (int i = 0; i < krzywa.w.Length; i++)
        {
            krzywa.w[i] = float.Parse(liczby[i]);
        }
    }

    /// <summary>
    /// Wyswietl tablice wezlow w polu tekstowym obiektu InputField
    /// </summary>
    private void WyswietlWezly()
    {
        wezly.text = "";        // wyczysc pole tekstowe

        foreach (float wezel in krzywa.U)
        {
            wezly.text += wezel.ToString() + " ";
        }
    }

    /// <summary>
    /// Zapisz wezly z pola tekstowego obiektu InputField do tablicy przechowujacej wezly krzywej
    /// </summary>
    public void ZapiszWezly()
    {
        string[] liczby =  wezly.text.Split(' ');   // podziel stringa na tablice stringow

        for (int i = 0; i < krzywa.U.Length; i++)
        {
            krzywa.U[i] = float.Parse(liczby[i]);
        }
    }

    /// <summary>
    /// Wywolaj funkcje rysujaca krzywa NURBS
    /// </summary>
    public void Rysuj()
    {
        krzywa.NURBS();
    }
    
    /// <summary>
    /// Zaladuj ponownie scene
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
