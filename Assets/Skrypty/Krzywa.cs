﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Krzywa : MonoBehaviour {

    private LineRenderer linia;

    public GameObject PunktKontrolny;           // punkt kontrolny jako obiekt do instancjonowania
    public List<GameObject> punktyKontrolne;    // lista punktow kontrolnych

    public float[] w;                           // waga, tyle samo pozycji co controlPoints.lenght
    public float[] U;                           // lista wezlow (knotow)
    private int m;                              // liczba wezlow

    public int max_n;                           // maksymalny stopien krzywej
    public int n;                               // obecny stopien krzywej
    
    void Start()
    {
        linia = GetComponent<LineRenderer>();           // zapisz komponent do zmiennej

        punktyKontrolne = new List<GameObject> { };     // inicializacja listy
    }

    /// <summary>
    /// Funkcja tworzy punkt kontrolny na scenie, dodaje go do listy i oblicza maksymalny stopien krzywej
    /// </summary>
    /// <param name="pozycja">(x, y)</param>
    public void StworzPunktKontrolny(Vector2 pozycja)
    {
        // Stworz punkt kontrolny na scenie i dodaj go do listy punktow kontrolnych
        punktyKontrolne.Add(Instantiate(PunktKontrolny, pozycja, Quaternion.identity));

        // Oblicz maksymalny stopien krzywej. Stopien wielomianu nie moze byc >= liczbie punktow kontrolnych.
        if (punktyKontrolne.Count >= 2)
        {
            max_n = punktyKontrolne.Count - 1;
        }
    }

    /// <summary>
    /// Funkcja tworzaca tablice wag punktow kontrolnych, tablice wezlow oraz zapisuje jej dlugosc do zmiennej
    /// </summary>
    public void ZatwierdzPunktyKontrolne()
    {
        // Stworz tablice wag poszczegolnych punktow kontrolnych
        w = new float[punktyKontrolne.Count];

        for (int i = 0; i < w.Length; i++)
        {
            w[i] = 1;
        }
        Debug.Log("Liczba wag punktow kontrolnych: " + w.Length);


        // Stworz tablice wezlow. Ilosc wezlow = ilosc punktow kontrolnych + stopien + 1
        U = new float[punktyKontrolne.Count + n + 1];

        float wartoscWezla = 1f / U.Length;

        for (int i = 0; i < U.Length; i++)
        {
            if (i == 0)
            {
                U[i] = 0;                       // pierwszy wezel = 0
            }
            else if (i == U.Length - 1)
            {
                U[i] = 1;                       // ostatni wezel = 1
            }
            else
            {
                U[i] = i * wartoscWezla;
            }
        }
        Debug.Log("Liczba wezlow: " + U.Length);


        // Zapisz liczbe wezlow
        m = U.Length;
    }

    /// <summary>
    /// Funkcja rysujaca krzywa NURBS
    /// </summary>
    public void NURBS()
    {
        linia.positionCount = 0;    // wyzeruj tablice punktow rysowanej krzywej
        int position = 0;           // indeks pozycji w tablice punktow

        Vector2 P;                  // wektor przechowujacy wspolrzedne obliczanego punktu do rysowania krzywej
        float dzielnik;

        for (float t = U[n]; t <= U[m - n - 1]; t += 0.01f)     // t nalezy do przedzialu [ u[n], u[m-n] )
        {
            P = new Vector2(0, 0);  // wyzeruj wektor P
            dzielnik = 0f;          // wyzeruj dzielnik

            // suma(i=0, m-n-1) {w[i] * N(i, n)(t)
            for (int i = 0; i < m - n - 1; i++)
            {
                dzielnik += N_in(i, n, t) * w[i];
            }

            // suma(i=0, m-n-1) {w[i] * P[i] * N(i, n)(t)
            for (int i = 0; i < m - n - 1; i++)
            {
                float nin = N_in(i, n, t);                                                  // zapisz wynik funkcji N_in do zmiennej
                P.x += w[i] * punktyKontrolne[i].transform.position.x * nin / dzielnik;     // oblicz osobno dla wspolrzednej x punktu
                P.y += w[i] * punktyKontrolne[i].transform.position.y * nin / dzielnik;     // oblicz osobno dla wspolrzednej y punktu
            }

            // Rysuj linie od punktu do punktu
            linia.positionCount += 1;                               // stworz nowe miejsce w tablicy
            linia.SetPosition(position++, new Vector2(P.x, P.y));
        }
    }

    /// <summary>
    /// Unormowana funkcja B-sklejana/
    /// Oblicza tylko niezerowe pozycje
    /// </summary>
    /// <param name="i">i-ty punkt kontrolny</param>
    /// <param name="n">stopien krzywej</param>
    /// <param name="t">krok</param>
    /// <returns>N_{i,n}(t)</returns>
    private float N_in(int i, int n, float t)
    {
        float[] N = new float[n + 1];
        float saved;
        float temp;
        
        // sprawdz dla specjalnych przypadkow
        if ((i == 0 && t == U[0]) || (i == (m - n - 2) && t == U[m - 1]))
        {
            return 1f;
        }

        // sprawdz, czy zachodzi wlasnosc lokalna
        if (t < U[i] || t >= U[i + n + 1])
        {
            return 0f;
        }

        // funkcje zerowego stopnia
        for (int j = 0; j <= n; j++)
        {
            if (t >= U[i + j] && t < U[i + j + 1])
            {
                N[j] = 1f;
            }
            else
            {
                N[j] = 0f;
            }
        }

        // oblicz "triangular table"
        for (int k = 1; k <= n; k++)
        {
            if (N[0] == 0f)
            {
                saved = 0f;
            }
            else
            {
                saved = ((t - U[i]) * N[0]) / (U[i + k] - U[i]);
            }

            for (int j = 0; j < n - k + 1; j++)
            {
                float Uleft = U[i + j + 1];
                float Uright = U[i + j + k + 1];

                if (N[j + 1] == 0f)
                {
                    N[j] = saved;       // zapisz
                    saved = 0f;         // wyzeruj
                }
                else
                {
                    temp = N[j + 1] / (Uright - Uleft);
                    N[j] = saved + (Uright - t) * temp;
                    saved = (t - Uleft) * temp;
                }
            }
        }
        return N[0];
    }


    #region Nie dzialajaca rekurencyjna funkcja B-Sklejana
    /// <summary>
    /// Unormowana funkcja B-sklejana stopnia n
    /// </summary>
    /// <param name="n">stopien krzywej</param>
    /// <param name="i">i-ty punkt kontrolny</param>
    /// <param name="t">krok</param>
    private float BSpline(int n, int i, float t)
    {
        if (n == 0 && (t >= U[i] && t < U[i + 1]))
        {
            return 1;
        }
        else if (n == 0) //&& (t < U[i] && t >= U[i + 1]))
        {
            return 0;
        }

        float Nlewy = ((t - U[i]) / (U[i + n] - U[i]));
        float test1 = BSpline(n - 1, i, t);
        Nlewy *= test1;

        float Nprawy = ((U[i + n + 1] - t) / (U[i + n + 1] - U[i + 1]));
        float test2 = BSpline(n - 1, i + 1, t);
        Nprawy *= test2;

        return Nlewy + Nprawy;
    }
    #endregion
}